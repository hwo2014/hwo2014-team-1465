package noobbot;

public class CarId {
	public String name;
	public String color;
	
	@Override
	public boolean equals(Object other) {
		if (other instanceof CarId) {
			CarId ci = (CarId)other;
			if (name.equals(ci.name) && color.equals(ci.color)) {
				return true;
			}
		}
		return false;
	}
}
