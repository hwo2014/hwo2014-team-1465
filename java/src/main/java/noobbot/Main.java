package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;

import fi.helsinki.cs.vtpblom.hwo.ai.AccelerationMeasurer;
import fi.helsinki.cs.vtpblom.hwo.ai.CarAI;
import fi.helsinki.cs.vtpblom.hwo.ai.RyanGosling;
import fi.helsinki.cs.vtpblom.hwo.ai.CrashMeasurer;
import fi.helsinki.cs.vtpblom.hwo.ai.Decision;
import fi.helsinki.cs.vtpblom.hwo.ai.KeimolaDriver;
import fi.helsinki.cs.vtpblom.hwo.ai.DecelerationMeasurer;
import fi.helsinki.cs.vtpblom.hwo.ai.SwitchSignal;
import fi.helsinki.cs.vtpblom.hwo.model.Car;
import fi.helsinki.cs.vtpblom.hwo.model.CarState;
import fi.helsinki.cs.vtpblom.hwo.model.Game;
import fi.helsinki.cs.vtpblom.hwo.model.TurboData;
import fi.helsinki.cs.vtpblom.hwo.physics.Environment;
import fi.helsinki.cs.vtpblom.hwo.protocol.LapFinishedMsg;
import fi.helsinki.cs.vtpblom.hwo.protocol.RaceMsg;
import fi.helsinki.cs.vtpblom.hwo.visualization.DataWindow;

public class Main {
	private static final boolean VISUALIZATION_ENABLED = false;
	
    private long totalAnalysisTime = 0;
    private long measureCount = 0;
    private long minAnalysisTime = Long.MAX_VALUE;
    private long maxAnalysisTime = Long.MIN_VALUE;

	
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
        
        socket.close();
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        Game game = new Game();
//        CarAI ai = new AccelerationMeasurer(game);
//        CarAI ai = new DecelerationMeasurer(game);
//        CarAI ai = new KeimolaDriver(game);
//        CarAI ai = new CrashMeasurer(game);
        CarAI ai = new RyanGosling(game);
        
        DataWindow visualization = null;
        if (VISUALIZATION_ENABLED) {
        	visualization = new DataWindow();
        	visualization.setGame(game);
        	visualization.setAI(ai);
        }
        
        send(join);
//        CreateRace cr = new CreateRace(join.name, join.key, "germany", "gigu", 1);
//        send(cr);
//        JoinRace jr = new JoinRace(join.name, join.key, "germany", "gigu", 1);
//        send(jr);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
            	long analysisStart = System.currentTimeMillis();

            	CarPositions carPosMsg = gson.fromJson(line, CarPositions.class);
            	game.updatePositions(carPosMsg);
            	ai.analyze();
  
            	recordAnalysisTime(analysisStart);
            	
            	if (carPosMsg.gameTick > 0) {
                	performAction(ai.getDecision(), carPosMsg.gameTick);
            	}
//                send(new Throttle(0.65));
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
                System.out.println(line);
//                send(new Ping());
            } else if (msgFromServer.msgType.equals("yourCar")) {
                System.out.println("Your car");
                System.out.println(line);
                YourCar yourCar = gson.fromJson(line, YourCar.class);
                Car c = game.addCar(yourCar.data);
                game.setMyCar(c);
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                System.out.println(line);
                GameInit gameInit = gson.fromJson(line, GameInit.class);
                RaceMsg race = gameInit.data.race;
                game.initRace(race);
                if (VISUALIZATION_ENABLED) {
                	visualization.start();
                }
//                send(new Ping());
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
                System.out.println(line);
                game.startRace();
                send(new Ping());
            } else if (msgFromServer.msgType.equals("crash")) {
                System.out.println("Crash");
                System.out.println(line);
                Crash c = gson.fromJson(line, Crash.class);
                game.carCrashed(c.data);
            } else if (msgFromServer.msgType.equals("spawn")) {
                System.out.println("Spawn");
                System.out.println(line);
                Spawn s = gson.fromJson(line, Spawn.class);
                game.carSpawned(s.data);
            } else if (msgFromServer.msgType.equals("turboAvailable")) {
                System.out.println("Turbo available");
                System.out.println(line);
                TurboAvailable ta = gson.fromJson(line, TurboAvailable.class);
                game.turboAvailable(ta.data);
            } else if (msgFromServer.msgType.equals("turboStart")) {
                System.out.println(line);
                TurboStart ts = gson.fromJson(line, TurboStart.class);
                game.turboStart(ts.data);
            } else if (msgFromServer.msgType.equals("turboEnd")) {
                System.out.println(line);
                TurboEnd te = gson.fromJson(line, TurboEnd.class);
                game.turboEnd(te.data);
            } else if (msgFromServer.msgType.equals("lapFinished")) {
//                System.out.println("Lap finished");
//                System.out.println(line);
                LapFinished f = gson.fromJson(line, LapFinished.class);
                game.lapFinished(f.data);
            } else if (msgFromServer.msgType.equals("finish")) {
                System.out.println("Finish");
                System.out.println(line);
                Finish f = gson.fromJson(line, Finish.class);
                game.carFinished(f.data);
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
                System.out.println(line);
                game.endRace();
                send(new Ping());
            } else if (msgFromServer.msgType.equals("tournamentEnd")) {
                System.out.println("Tournament end");
                System.out.println(line);
                game.endGame();
            } else {
                System.out.println("Other: " + msgFromServer.msgType);
                System.out.println(line);
                System.out.println("my car: " + gson.toJson(game.getMyCar()));
//                send(new Ping());
            }
        }
        
        double analysisAvg = 1.0*totalAnalysisTime/measureCount;
        System.out.format("Analysis results:\n");
        System.out.format("Total time: %d / Min %d / Max %d / Avg %.2f\n",
        		totalAnalysisTime, minAnalysisTime, maxAnalysisTime, analysisAvg);
    }
    
    private void recordAnalysisTime(long startTime) {
    	long analysisDuration = System.currentTimeMillis() - startTime;
    	measureCount++;
    	totalAnalysisTime += analysisDuration;
    	if (analysisDuration < minAnalysisTime) {
    		minAnalysisTime = analysisDuration;
    	}
    	if (analysisDuration > maxAnalysisTime) {
    		maxAnalysisTime = analysisDuration;
    	}
    	
    }

    private void performAction(Decision d, int gameTick) {
    	if (d.getSwitchSignal() == SwitchSignal.LEFT) {
    		d.setSwitchSignal(SwitchSignal.RECEIVED);
    		SwitchLane sl = new SwitchLane("Left");
    		send(sl);
     		System.out.println("Left signalled");
    	}
    	else if (d.getSwitchSignal() == SwitchSignal.RIGHT) {
    		d.setSwitchSignal(SwitchSignal.RECEIVED);
    		SwitchLane sl = new SwitchLane("Right");
    		send(sl);
     		System.out.println("Right signalled");
    	}
    	else if (d.isFireTurbo()) {
    		d.setFireTurbo(false);
    		Turbo turbo = new Turbo("VVVRRRROOOOOM");
    		send(turbo);
    		System.out.println("Turbo signalled");
    	}
    	else {
        	Throttle t = new Throttle(d.getThrottle(), gameTick);
            send(t);
//            System.out.format("Throttle sent:\n%s\n", t.toJson());
    	}
    }
    
    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
    
    protected Integer gameTick() {
    	return null;
    }
}

class MsgWrapper {
    public final String msgType;
    public final Object data;
    public final Integer gameTick;

    MsgWrapper(final String msgType, final Object data, final Integer gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData(), sendMsg.gameTick());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class CreateRace extends SendMsg {
	public final Join botId;
	public final String trackName;
	public final String password;
	public final int carCount;
	
	CreateRace(String name, String key, String trackName, String pwd, int carCount) {
		this.botId = new Join(name, key);
		this.trackName = trackName;
		this.password = pwd;
		this.carCount = carCount;
	}
	
    @Override
	protected String msgType() {
		return "createRace";
	}
	
}

class JoinRace extends CreateRace {
	JoinRace(String name, String key, String trackName, String pwd, int carCount) {
		super(name, key, trackName, pwd, carCount);
	}
	
    @Override
	protected String msgType() {
		return "joinRace";
	}
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;
    private int gameTick;

    public Throttle(double value, int gameTick) {
        this.value = value;
        this.gameTick = gameTick;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }

    @Override
    protected Integer gameTick() {
        return gameTick;
    }
}

class SwitchLane extends SendMsg {
    private String value;

    public SwitchLane(String value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}

class Turbo extends SendMsg {
	private String message;
	
	public Turbo(String message) {
		this.message = message;
	}

	@Override
    protected Object msgData() {
        return message;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
}


class YourCar extends SendMsg {
	CarId data;
		
	@Override
	protected String msgType() {
		return "yourCar";
	}
}

class GameInit extends SendMsg {
	RaceData data;
	
	static class RaceData {
		RaceMsg race;
	}
	
	@Override
	protected String msgType() {
		return "gameInit";
	}
}

class Crash extends SendMsg {
	CarId data;
	
	@Override
	protected String msgType() {
		return "crash";
	}
}

class Spawn extends SendMsg {
	CarId data;
	
	@Override
	protected String msgType() {
		return "spawn";
	}
}



class LapFinished extends SendMsg {
	LapFinishedMsg data;
	String gameId;
	int gameTick;
	
	@Override
	protected String msgType() {
		return "lapFinished";
	}
}

class Finish extends SendMsg {
	CarId data;
	String gameId;
	int gameTick;
	
	@Override
	protected String msgType() {
		return "finish";
	}
}

class TurboAvailable extends SendMsg {
	TurboData data;
	
	@Override
	protected String msgType() {
		return "turboAvailable";
	}
}

class TurboStart extends SendMsg {
	CarId data;

	@Override
	protected String msgType() {
		return "turboStart";
	}
}

class TurboEnd extends SendMsg {
	CarId data;

	@Override
	protected String msgType() {
		return "turboEnd";
	}
}
