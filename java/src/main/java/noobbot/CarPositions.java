package noobbot;

import java.util.List;


public class CarPositions extends SendMsg {
	public String gameId;
	public int gameTick;
	public List<CarPosData> data;
	
	@Override
	protected String msgType() {
		return "carPositions";
	}

}
