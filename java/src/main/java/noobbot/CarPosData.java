package noobbot;

public class CarPosData {
	public CarId id;
	public double angle;
	public PiecePos piecePosition;

	public static class PiecePos {
		public int pieceIndex;
		public double inPieceDistance;
		public Lane lane;
		public int lap;

		public static class Lane {
			public int startLaneIndex;
			public int endLaneIndex;
		}
	}
}
