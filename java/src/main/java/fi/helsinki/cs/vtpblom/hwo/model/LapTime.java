package fi.helsinki.cs.vtpblom.hwo.model;

import lombok.Data;

@Data
public class LapTime {
	private int lap;
	private int ticks;
	private int millis;
	private boolean qualifying;
}
