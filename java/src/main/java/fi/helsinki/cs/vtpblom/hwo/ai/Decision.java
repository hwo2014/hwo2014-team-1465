package fi.helsinki.cs.vtpblom.hwo.ai;

import lombok.Data;

@Data
public class Decision {
	private double throttle;
	private SwitchSignal switchSignal = SwitchSignal.NO_SIGNAL;
	private boolean fireTurbo = false;	
}
