package fi.helsinki.cs.vtpblom.hwo.ai;

import fi.helsinki.cs.vtpblom.hwo.model.Car;
import fi.helsinki.cs.vtpblom.hwo.model.CarState;
import fi.helsinki.cs.vtpblom.hwo.model.Game;

public class CrashMeasurer implements CarAI {
	private static final double ANGLE_REC_THRHOLD = 40.0;
	private Game game;
	private Decision decision = new Decision();
	private State state = State.MEASURING;

	public CrashMeasurer(Game game) {
		this.game = game;
	}
	
	@Override
	public void analyze() {
		Car myCar = game.getMyCar();
		CarState carState = myCar.getCurrState();
		
		if (state != State.CRASHED && carState.isCrashed()) {
			state = State.CRASHED;
			game.stopRecording();
		}
		else if (myCar.getPrevState() != null) {
			if (Math.abs(carState.getAngle()) > ANGLE_REC_THRHOLD && Math.abs(myCar.getPrevState().getAngle()) <= ANGLE_REC_THRHOLD) {
				game.startRecording();
			}
			else if (Math.abs(carState.getAngle()) <= ANGLE_REC_THRHOLD && Math.abs(myCar.getPrevState().getAngle()) > ANGLE_REC_THRHOLD) {
				game.stopRecording();
			}
		}
		
		switch (state) {
			case MEASURING:
				if (game.getGameTick() > 35) {
					state = State.SIX;
				}
				break;
			case SIX :
				if (carState.getSpeed() > 10*state.getThrottle()-1.5) {
					state = State.SEVEN;
				}
				break;
			case SEVEN :
				if (carState.getSpeed() > 10*state.getThrottle()-1.5) {
					state = State.EIGHT;
				}
				break;
			case EIGHT :
				if (carState.getSpeed() > 10*state.getThrottle()-1.5) {
					state = State.NINE;
				}
				break;
			case NINE :
				if (carState.getSpeed() > 10*state.getThrottle()-1.5) {
					state = State.TEN;
				}
				break;
			case CRASHED :
				if (!carState.isCrashed()) {
					state = State.SIX;
				}
				break;
		}
	}

	@Override
	public Decision getDecision() {
		decision.setThrottle(state.getThrottle());
		return decision;
	}
	
	private enum State {
		MEASURING(1.0),
		SIX(0.6),
		SEVEN(0.7),
		EIGHT(0.8),
		NINE(0.9),
		TEN(1.0),
		CRASHED(0.0);
		
		private double throttle;
		State(double throttle) {
			this.throttle = throttle;
		}
		
		double getThrottle() {
			return throttle;
		}
	}
}
