package fi.helsinki.cs.vtpblom.hwo.protocol;

import fi.helsinki.cs.vtpblom.hwo.model.LapTime;
import fi.helsinki.cs.vtpblom.hwo.model.RaceTime;
import noobbot.CarId;

public class LapFinishedMsg {
	public CarId car;
	public LapTime lapTime;
	public RaceTime raceTime;
}
