package fi.helsinki.cs.vtpblom.hwo.protocol;

public class DimensionMsg {
	public double length;
	public double width;
	public double guideFlagPosition;
}
