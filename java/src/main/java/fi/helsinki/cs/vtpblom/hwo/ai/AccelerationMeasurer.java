package fi.helsinki.cs.vtpblom.hwo.ai;

import fi.helsinki.cs.vtpblom.hwo.model.Game;

public class AccelerationMeasurer implements CarAI {
	private Game game;
	private static final double EPSILON = 0.05;
	private Decision decision = new Decision();
	private State state = State.DRIVE_CAREFULLY;
	private int wait = 0;
	
	public AccelerationMeasurer(Game game) {
		this.game = game;
	}

	@Override
	public void analyze() {
		switch (state) {
			case DRIVE_CAREFULLY :
				if (game.getMyCar().getCurrState().getPieceIndex() == 35) {
					System.out.format("(%d) Time to stop\n", game.getGameTick());
					state = State.SLOW_TO_HALT;
				}
				break;
			case SLOW_TO_HALT :
				if (game.getMyCar().getCurrState().getSpeed() < EPSILON) {
					System.out.format("(%d) Acceleration test starts\n", game.getGameTick());
					state = State.ACCELERATE;
				}
				break;
			case ACCELERATE :
				if (Math.abs(game.getMyCar().getCurrState().getSpeed()-game.getMyCar().getPrevState().getSpeed()) < EPSILON) {
					System.out.format("(%d) At full speed. Deceleration test starts\n", game.getGameTick());
					state = State.DECELERATE;
				}
				break;
			case DECELERATE:
				if (game.getMyCar().getCurrState().getSpeed() < EPSILON) {
					System.out.format("(%d) Full stop\n", game.getGameTick());
					state = State.DRIVE_CAREFULLY;
				}
				break;
		}
		
	}

	@Override
	public Decision getDecision() {
		decision.setThrottle(state.getThrottle());
		return decision;
	}
	
	private enum State {
		DRIVE_CAREFULLY(0.65),
		SLOW_TO_HALT(0.0),
		ACCELERATE(1.0),
		DECELERATE(0.0);
		
		private double throttle;
		State(double throttle) {
			this.throttle = throttle;
		}
		
		double getThrottle() {
			return throttle;
		}
	}
}
