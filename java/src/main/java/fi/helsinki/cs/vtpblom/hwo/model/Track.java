package fi.helsinki.cs.vtpblom.hwo.model;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;

@Data
public class Track {
	private String id;
	private String name;
	private TrackPiece[] pieces;
	private Integer[] laneDistances;
	
	private Map<Integer, Map<Integer, Double>> pieceLaneLengths;
	private Map<Integer, Map<Integer, Integer>> pieceLaneRadii;
	
	public void calculatePieceDetails() {
		System.out.println("piece detail calculation started");
		this.pieceLaneLengths = new HashMap<Integer, Map<Integer, Double>>();
		this.pieceLaneRadii = new HashMap<Integer, Map<Integer, Integer>>();
		for (int i=0; i<pieces.length; i++) {
			TrackPiece piece = pieces[i];
			piece.setId(i);
			piece.setCorner(piece.getRadius() > 0); // Only corners have nonzero radius
			
			Map<Integer, Double> laneLengths = new HashMap<Integer, Double>();
			Map<Integer, Integer> laneRadii = new HashMap<Integer, Integer>();
			for (int j = 0; j < laneDistances.length; j++) {
				double laneLength = piece.getLength();
				int laneRadius = 0;
				if (piece.isCorner()) {
					laneLength = calculateCornerLength(piece, laneDistances[j]);
					laneRadius = calculateCornerRadius(piece, laneDistances[j]);
				}
				laneLengths.put(j, laneLength);
				laneRadii.put(j, laneRadius);
			}
			pieceLaneLengths.put(i, laneLengths);
			pieceLaneRadii.put(i, laneRadii);
		}
		System.out.println("piece details calculated");
	}
	
	public double getPieceLength(int pieceIdx, int laneIdx) {
		return pieceLaneLengths.get(pieceIdx).get(laneIdx);
	}
	public int getPieceRadius(int pieceIdx, int laneIdx) {
		return pieceLaneRadii.get(pieceIdx).get(laneIdx);
	}
	
	public double getPieceLength(Car car) {
		return pieceLaneLengths.get(car.getCurrState().getPieceIndex()).get(car.getCurrState().getStartLane());
	}
	public int getPieceRadius(Car car) {
		return pieceLaneRadii.get(car.getCurrState().getPieceIndex()).get(car.getCurrState().getStartLane());
	}
	public double getPieceAngle(Car car) {
		return pieces[car.getCurrState().getPieceIndex()].getAngle();
	}
	
	private double calculateCornerLength(TrackPiece piece, int laneDistance) {
		double radius = calculateCornerRadius(piece, laneDistance);
		return 2*Math.PI*radius * (Math.abs(piece.getAngle())/360.0);
	}

	private int calculateCornerRadius(TrackPiece piece, int laneDistance) {
		int radius = piece.getRadius();
		if (piece.getAngle() < 0) {
			radius += laneDistance;
		}
		else {
			radius -= laneDistance;
		}
		return radius;
	}
}
 