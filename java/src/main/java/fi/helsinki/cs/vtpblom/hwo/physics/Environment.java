package fi.helsinki.cs.vtpblom.hwo.physics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import fi.helsinki.cs.vtpblom.hwo.model.Car;
import fi.helsinki.cs.vtpblom.hwo.model.CarState;
import fi.helsinki.cs.vtpblom.hwo.model.Game;
import fi.helsinki.cs.vtpblom.hwo.model.LaneNode;

public class Environment implements Observer {
	public static final int AIR_RES_MEASURING_TIME = 24; // ticks
	public static final double SLOWEST_SPEED = 3.0;
	public static final double HIGHEST_SPEED = 99.9;

	private double speedFactor = 1.0;
	private Map<Integer, Double> cornerSpeeds = new HashMap<Integer, Double>();
	private double crashAngle = 57.0;
	
	private Game game; // Game to analyze
	private List<Double> data = new ArrayList<Double>(); // Measures
	private double power; // Measured power of the engine
	private boolean powerMeasured = false;
	private double airRes; // Measured air resistance
	private boolean airResMeasured = false;
	
	public Environment(Game game) {
		this.game = game;
		game.addObserver(this);
		
		cornerSpeeds.put(10, 1.5);
		cornerSpeeds.put(40, 4.0);
		cornerSpeeds.put(60, 5.0);
		cornerSpeeds.put(90, 6.3);
		cornerSpeeds.put(110, 7.1);
		cornerSpeeds.put(210, 9.5);
		cornerSpeeds.put(400, 18.0);
	}

	@Override
	public void update(Observable o, Object arg) {
		double carSpeed = this.game.getMyCar().getCurrState().getSpeed();
		if (!powerMeasured && carSpeed > 0) {
			powerMeasured = true;
			power = carSpeed;
		}
		if (data.size() < AIR_RES_MEASURING_TIME && carSpeed > 0) {
			data.add(carSpeed);
		}
		else if (!airResMeasured && data.size() >= AIR_RES_MEASURING_TIME) {
			airResMeasured = true;
			calculateAirRes();
		}
	}
	
	public void learnFromOwnCrash(Car myCar, List<Car> allCars) {
		if (wasPushed(myCar, allCars)) {
			System.out.println("I think I was pushed off the track!");
		}
		else {
			speedFactor = 0.95 * speedFactor; // Lower speed thresholds
			System.out.format("I crashed due to driving too fast, lowering thresholds to %.2f\n", speedFactor);
		}
	}

	public void learnFromCrash(Car crashedCar) {
//		List<CarState> lastStates = crashedCar.getLastNStates(2, true);
//		double angle1 = Math.abs(lastStates.get(0).getAngle());
//		double angle2 = Math.abs(lastStates.get(1).getAngle());
//		double estAngle = angle2 + (angle2-angle1)/2.0;
//		if (angle2 > angle1 && estAngle < crashAngle) {
//			System.out.format("Environment: Lowered crash angle from %.2f to %.2f\n", crashAngle, estAngle);
//			this.crashAngle = estAngle;
//		}
	}
	
	public double getAirRes() {
		return airRes;
	}
	
	public double getCrashAngle() {
		return crashAngle;
	}
	
	public double getWarnAngle() {
		return crashAngle - 15.0;
	}
	
	public double getNodeSpeed(LaneNode node) {
		if (!node.isCorner()) {
			return HIGHEST_SPEED;
		}
		
		int radius = node.getRadius();
		
		double targetSpeed = 0.0;
		if (cornerSpeeds.containsKey(radius)) {
			targetSpeed = cornerSpeeds.get(radius);
		}
		else {
			targetSpeed = calculateCornerSpeed(radius);
			cornerSpeeds.put(radius, targetSpeed);
		}
		
		return targetSpeed * speedFactor;		
	}
	
	/**
	 * Calculates distance it takes to decelerate from current speed to below target speed, if throttle 0
	 */
	public double getDistanceToSpeed(double currSpeed, double targetSpeed) {
		double dist = 0.0;
		while (currSpeed > targetSpeed) {
			dist += currSpeed;
			currSpeed -= Math.pow((currSpeed*airRes), 1+airRes);
		}
		return dist;
	}
	
	/**
	 * Calculates adequate throttle to accelerate/decelerate from current speed to target speed.
	 * If impossible to reach target speed in one tick, returns 1.0. Likewise, if impossible to
	 * decelerate to target speed, returns 0.0.
	 */
	public double getThrottleToSpeed(double currSpeed, double targetSpeed, double turboFactor) {
		double pwr = this.power * turboFactor;
		double deceleration = Math.pow((currSpeed*airRes), 1+airRes);
		double accRequired = targetSpeed - currSpeed + deceleration;
		double throttle = accRequired / pwr;
		throttle = Math.max(throttle, 0.0);
		throttle = Math.min(throttle, 1.0);
		return throttle;
	}
	
	private void calculateAirRes() {
//		System.out.println("Calculating air resistance");
		double dataSum = 0.0;
		double prevSpeed = 0.0;
		for (double speed : data) {
			double acc = speed-prevSpeed;
			dataSum += acc;
			prevSpeed = speed;
//			System.out.format("Measure: %.2f spd, %.4f acc, %.5f data sum\n", speed, acc, dataSum);
		}
		this.airRes = iterateAirRes(15, 1, 0.5, dataSum);
		System.out.println("Calculated air resistance: " + this.airRes);
	}
	
	private double iterateAirRes(int count, double guess, double step, double sum) {
		double sumOfEstimates = 0.0;
		double prevSpeed = 0.0;
		for (int i=0; i<data.size(); i++) {
			double estimate = power - Math.pow((prevSpeed*guess), 1+guess);
//			System.out.format("%.3f ", estimate);
			sumOfEstimates += estimate;
			prevSpeed += estimate;
		}
//		System.out.format("\nIterating. Count: %d / guess %.4f / sumOfEst %.5f\n", count, guess, sumOfEstimates);
		double nextGuess = guess - step;
		if (sumOfEstimates > sum) {
			nextGuess = guess + step;
		}
		if (count == 0) {
			return nextGuess;
		}
		else {
			return iterateAirRes(count-1, nextGuess, step/2.0, sum);
		}
	}
	
	private double calculateCornerSpeed(int radius) {
		int lowRad = Integer.MIN_VALUE;
		int highRad = Integer.MAX_VALUE;
		for (int rad : cornerSpeeds.keySet()) {
			if (rad > lowRad && rad < radius) {
				lowRad = rad;
			}
			if (rad < highRad && rad > radius) {
				highRad = rad;
			}
		}
		Double lower = cornerSpeeds.get(lowRad);
		Double higher = cornerSpeeds.get(highRad);
		if (lower == null) {
			return higher;
		}
		else if (higher == null) {
			return lower;
		}
		return lower + (1.0*radius/highRad)*(higher-lower);
	}
	
	private boolean wasPushed(Car crashed, List<Car> allCars) {
		for (Car c : allCars) {
			if (c.getCurrState().isOnTrack() && !c.getId().equals(crashed.getId())) {
				double distance = game.getLaneNetwork().getDistanceBetween(c, crashed);
				if (distance < (c.getLength()*1.3)) {
					// Seems the crashed car was pushed off
					System.out.format("%s was pushed off the track by %s\n",
							crashed.getId().color, c.getId().color);
					return true;
				}
			}
		}
		return false;
	}

}
