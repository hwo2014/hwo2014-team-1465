package fi.helsinki.cs.vtpblom.hwo.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import fi.helsinki.cs.vtpblom.hwo.physics.Environment;
import fi.helsinki.cs.vtpblom.hwo.protocol.CarMsg;
import fi.helsinki.cs.vtpblom.hwo.protocol.LaneMsg;
import fi.helsinki.cs.vtpblom.hwo.protocol.LapFinishedMsg;
import fi.helsinki.cs.vtpblom.hwo.protocol.RaceMsg;
import fi.helsinki.cs.vtpblom.hwo.protocol.SessionMsg;
import fi.helsinki.cs.vtpblom.hwo.protocol.TrackMsg;
import noobbot.CarId;
import noobbot.CarPosData;
import noobbot.CarPositions;
import lombok.Data;
import lombok.ToString;

@Data
@ToString(exclude="stateRecord")
public class Game extends Observable {
	public static final boolean RECORDING_ENABLED = false;
	
	private String gameId;
	private long gameTick;
//	private Race race; // TODO: Joku vastaava olio
	private Environment environment;
	private Track track;
	private LaneNetwork laneNetwork;
	private List<Car> cars = new ArrayList<Car>();
	private Car myCar;
	private boolean qualifying;
	private int laps;
	private int maxLapTimeMs;
	private boolean quickRace;
	private int durationMs;

	private transient boolean recording;
	private transient StringBuilder stateRecord;
	
	public Car getCar(CarId carId) {
		for (Car c : cars) {
			if (c.getId().equals(carId)) {
				return c;
			}
		}
		return null;
	}

	public Car addCar(CarId carId) {
		Car c = getCar(carId);
		if (c == null) {
			c = new Car(this, carId);
			cars.add(c);
			System.out.println("Car "+ carId.name + " added");
		}
		return c;
	}
	
	public void setMyCar(Car c) {
		this.myCar = c;
		System.out.println("My car is " + c.getId().color);
	}
	
	public void initRace(RaceMsg race) {
		parseSessionData(race.raceSession);
		if (track == null) {
			parseCarData(race.cars);
			parseTrackData(race.track);
			this.laneNetwork = new LaneNetwork(track);
			this.environment = new Environment(this);
		}
		// TODO reset car stats
	}
	
	public void startRace() {
		// TODO
	}

	public void updatePositions(CarPositions positions) {
		setGameTick(positions.gameTick);
		for (CarPosData carPos : positions.data) {
			Car car = getCar(carPos.id);
			car.addData(positions.gameTick, carPos);
		}
		setChanged();
		notifyObservers();

		if (RECORDING_ENABLED && recording) {
			stateRecord.append(currStatusString());
		}
	}
	
	public void carCrashed(CarId car) {
		Car crashedCar = getCar(car);
		crashedCar.getCurrState().setCrashed(true);
		if (myCar.getId().equals(car)) {
			System.out.println("I crashed! Details: " + myCar);
			environment.learnFromOwnCrash(myCar, cars);
		}
		environment.learnFromCrash(crashedCar);
	}

	public void carSpawned(CarId car) {
		getCar(car).getCurrState().setCrashed(false);
	}
	
	public void turboAvailable(TurboData turboData) {
		if (myCar.getCurrState().isOnTrack()) {
			myCar.getCurrState().setTurboAvailable(turboData);
		}
	}
	
	public void turboStart(CarId carId) {
		if (carId.equals(myCar.getId())) {
			System.out.println("My turbo was started!");
			CarState myState = myCar.getCurrState();
    		myState.setTurboRemaining(myState.getTurboAvailable().getTurboDurationTicks());
    		myState.setTurboFactor(myState.getTurboAvailable().getTurboFactor());
    		myState.setTurboAvailable(null);
		}
	}
	public void turboEnd(CarId carId) {
		if (carId.equals(myCar.getId())) {
			System.out.println("My turbo ended :(");
		}
	}

	public void lapFinished(LapFinishedMsg lapFinished) {
		lapFinished.lapTime.setQualifying(qualifying);
		getCar(lapFinished.car).addLapData(lapFinished.lapTime, lapFinished.raceTime);
	}
	
	public void carFinished(CarId car) {
		getCar(car).getCurrState().setFinished(true);		
	}

	public void endRace() {
		// TODO
	}
	
	public void endGame() {
		// TODO
	}
	
	public void startRecording() {
		if (!recording) {
			recording = true;
			stateRecord = new StringBuilder();
		}
	}
	
	public void stopRecording() {
		recording = false;
		if (RECORDING_ENABLED && stateRecord != null) {
			System.out.println(stateRecord.toString());
		}
	}

	private void parseSessionData(SessionMsg session) {
		this.laps = session.laps;
		this.maxLapTimeMs = session.maxLapTimeMs;
		this.quickRace = session.quickRace;
		this.durationMs = session.durationMs;
		if (this.durationMs > 0) {
			qualifying = true;
		}
	}
	
	private void parseCarData(CarMsg[] carMsgs) {
		for (CarMsg cm : carMsgs) {
			Car car = addCar(cm.id);
			car.setLength(cm.dimensions.length);
			car.setWidth(cm.dimensions.width);
			car.setGuideFlagPos(cm.dimensions.guideFlagPosition);
		}
	}
	
	private void parseTrackData(TrackMsg trackMsg) {
		this.track = new Track();
		track.setId(trackMsg.id);
		track.setName(trackMsg.name);
		Integer[] laneDistances = new Integer[trackMsg.lanes.length];
		for (LaneMsg lm : trackMsg.lanes) {
			laneDistances[lm.index] = lm.distanceFromCenter;
		}
		track.setLaneDistances(laneDistances);
		track.setPieces(trackMsg.pieces);
		track.calculatePieceDetails();
	}

	private String currStatusString() {
		CarState cs = myCar.getCurrState();
		return String.format("(%4d) Piece: %2d [L%4.1f R%3d A%4.1f] / Dist: %5.2f (%2d%%) | Spd: %6.4f | Ang: %5.2f | Acc: %7.5f\n", 
				cs.getTick(), cs.getPieceIndex(),
				track.getPieceLength(myCar), track.getPieceRadius(myCar), track.getPieceAngle(myCar),
				cs.getInPieceDistance(), (int)(100.0*cs.getInPieceDistance()/track.getPieceLength(myCar)),
				cs.getSpeed(), cs.getAngle(), cs.getAcceleration());
	}
	
	private void printSummary() {
		List<CarState> myHistory = myCar.getPosHistory();
		int skip = 0;
		for (CarState cs : myHistory) {
			if (skip > 0) {
				skip--;
			}
			else {
				System.out.format("(%4d) Piece: %2d / Len: %5.2f | Spd: %6.4f | Ang: %5.2f | Acc: %7.5f\n", 
						cs.getTick(), cs.getPieceIndex(), cs.getInPieceDistance(),
						cs.getSpeed(), cs.getAngle(), cs.getAcceleration());
				skip = 0;
			}
		}
	}
}
