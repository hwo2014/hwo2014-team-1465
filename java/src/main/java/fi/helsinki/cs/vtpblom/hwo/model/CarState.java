package fi.helsinki.cs.vtpblom.hwo.model;

import lombok.Data;

@Data
public class CarState {
	// Timestamp of data
	private long tick;
	// In-game data
	private boolean crashed;
	private boolean finished;
	private int pieceIndex;
	private TrackPiece piece;
	private double inPieceDistance;
	private int startLane;
	private int endLane;
	private int lap;
	private double angle;
	private TurboData turboAvailable;
	private int turboRemaining;
	private double turboFactor = 1.0;

	// Calculated
	private double speed = 0.0;
	private double acceleration = 0.0;
	
	public boolean isOnTrack() {
		return (!crashed && !finished);
	}
}
