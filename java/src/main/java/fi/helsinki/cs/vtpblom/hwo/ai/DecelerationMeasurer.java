package fi.helsinki.cs.vtpblom.hwo.ai;

import fi.helsinki.cs.vtpblom.hwo.model.Game;

public class DecelerationMeasurer implements CarAI {
	private static final double EPSILON = 0.05;
	private Game game;
	private Decision decision = new Decision();
	private State state = State.DRIVE_CAREFULLY;

	public DecelerationMeasurer(Game game) {
		this.game = game;
	}
	
	@Override
	public void analyze() {
		switch (state) {
			case DRIVE_CAREFULLY :
				if (game.getMyCar().getCurrState().getPieceIndex() == 35) {
					System.out.format("(%d) Time to floor it\n", game.getGameTick());
					state = State.ACCELERATE;
					game.startRecording();
				}
				break;
			case ACCELERATE :
				if (game.getMyCar().getCurrState().getSpeed() > 9.0) {
					System.out.format("(%d) At high speed. Deceleration test starts\n", game.getGameTick());
					state = State.DECELERATE;
				}
				break;
			case DECELERATE :
				if (game.getMyCar().getCurrState().getSpeed() < 1.0) {
					System.out.format("(%d) Almost stopped, end recording.\n", game.getGameTick());
					System.out.println("Then just chillin' to finish.");
					state = State.DRIVE_ON;
					game.stopRecording();
				}
				break;
			case DRIVE_ON:
				// Drive on
				break;
		}
	}

	@Override
	public Decision getDecision() {
		decision.setThrottle(state.getThrottle());
		return decision;
	}
	
	private enum State {
		DRIVE_CAREFULLY(0.65),
		ACCELERATE(1.0),
		DECELERATE(0.0),
		DRIVE_ON(0.65);
		
		private double throttle;
		State(double throttle) {
			this.throttle = throttle;
		}
		
		double getThrottle() {
			return throttle;
		}
	}
}
