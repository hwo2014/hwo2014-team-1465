package fi.helsinki.cs.vtpblom.hwo.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.helsinki.cs.vtpblom.hwo.model.LaneNode.LaneDir;

public class LaneNetwork {
	Track track;
	Map<Integer, Map<Integer, LaneNode>> nodes;
	
	public LaneNetwork(Track track) {
		this.track = track;
		parseNetwork();
	}

	public LaneNode getCurrNode(Car car) {
		return nodes.get(car.getCurrState().getPieceIndex()).get(car.getCurrState().getEndLane());
	}
	
	public List<LaneNode> getNodesAhead(Car car, double distance) {
		List<LaneNode> res = new ArrayList<LaneNode>();
		LaneNode ln = getCurrNode(car);
		double inPieceDist = car.getCurrState().getInPieceDistance();
		return gatherNodesAhead(res, ln, distance+inPieceDist);
	}
	
	/**
	 * Only returns sensible results if the cars are close to each other
	 */
	public double getDistanceBetween(Car a, Car b) {
		LaneNode nodeA = getCurrNode(a);
		LaneNode nodeB = getCurrNode(b);
		double inPieceA = a.getCurrState().getInPieceDistance();
		double inPieceB = b.getCurrState().getInPieceDistance();
		if (nodeA.getLane() == nodeB.getLane()) {
			if (nodeA.getId() == nodeB.getId()) {
				return Math.abs(inPieceA-inPieceB);
			}
			else if (nodeA.getNextNode().getId() == nodeB.getId()) {
				return nodeA.getLength()-inPieceA + inPieceB;
			}
			else if (nodeB.getNextNode().getId() == nodeA.getId()) {
				return nodeB.getLength()-inPieceB + inPieceA;
			}
		}
		return Double.MAX_VALUE; // Cars are not very close to each other
	}
	
	private List<LaneNode> gatherNodesAhead(List<LaneNode> gathered, LaneNode curr, double distance) {
		if (distance < 0) {
			return gathered;
		}
		gathered.add(curr);
		return gatherNodesAhead(gathered, curr.getNextNode(LaneDir.STRAIGHT), distance-curr.getLength());
	}
	
	private void parseNetwork() {
		int laneCount = track.getLaneDistances().length;
		this.nodes = new HashMap<Integer, Map<Integer,LaneNode>>();
		for (int pieceId=0; pieceId < track.getPieces().length; pieceId++) {
			Map<Integer, LaneNode> pieceLanes = new HashMap<Integer, LaneNode>();
			for (int laneId=0; laneId < laneCount; laneId++) {
				LaneNode node = createNode(pieceId, laneId);
				pieceLanes.put(laneId, node);
			}
			nodes.put(pieceId, pieceLanes);
			if (pieceId > 0) {
				createEdges(nodes.get(pieceId-1), pieceLanes);
			}
		}
		createEdges(nodes.get(track.getPieces().length-1), nodes.get(0));
		
//		for (Integer piece : nodes.keySet()) {
//			for (Integer lane : nodes.get(piece).keySet()) {
//				LaneNode ln = nodes.get(piece).get(lane);
//				System.out.format("[Parse] Node id: %d / lane %d | hasLeft? %s | hasRight? %s | hasStraight %s\n",
//						ln.getId(), ln.getLane(),
//						ln.getNextNode(LaneDir.LEFT) != null, 
//						ln.getNextNode(LaneDir.RIGHT) != null, 
//						ln.getNextNode(LaneDir.STRAIGHT) != null);
//			}
//		}

	}
	
	private LaneNode createNode(int pieceId, int laneId) {
		TrackPiece piece = track.getPieces()[pieceId];
		LaneNode node = new LaneNode(piece);
		node.setLane(laneId);
		node.setRadius(track.getPieceRadius(pieceId, laneId));
		node.setLength(track.getPieceLength(pieceId, laneId));
		
		return node;
	}
	
	private void createEdges(Map<Integer, LaneNode> prevNodes, Map<Integer, LaneNode> nextNodes) {
		for (int laneId : prevNodes.keySet()) {
			LaneNode parent = prevNodes.get(laneId);
			LaneNode straightNext = nextNodes.get(laneId); // Next piece along same lane
			parent.addNextNode(LaneNode.LaneDir.STRAIGHT, straightNext, parent.getLength());
			if (parent.isSwitch()) {
				LaneNode leftNext = nextNodes.get(laneId-1); // Next piece if switching left
				LaneNode rightNext = nextNodes.get(laneId+1); // Next piece if switching right
				if (leftNext != null) {
					parent.addNextNode(LaneNode.LaneDir.LEFT, leftNext, parent.getLength());
//					System.out.format("Left added from id %d --> id %d\n", parent.getId(), leftNext.getId());
				}
				if (rightNext != null) {
					parent.addNextNode(LaneNode.LaneDir.RIGHT, rightNext, parent.getLength());
//					System.out.format("Right added from id %d --> id %d\n", parent.getId(), rightNext.getId());
				}
			}
		}
		
	}
}
