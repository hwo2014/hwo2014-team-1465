package fi.helsinki.cs.vtpblom.hwo.protocol;

public class SessionMsg {
	public int laps;
	public int maxLapTimeMs;
	public boolean quickRace;
	public int durationMs;
}
