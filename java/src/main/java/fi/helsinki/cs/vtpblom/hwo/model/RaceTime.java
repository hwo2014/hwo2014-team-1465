package fi.helsinki.cs.vtpblom.hwo.model;

import lombok.Data;


@Data
public class RaceTime {
	private int laps;
	private int ticks;
	private int millis;
}
