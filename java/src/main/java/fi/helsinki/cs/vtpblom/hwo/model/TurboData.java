package fi.helsinki.cs.vtpblom.hwo.model;

import lombok.Data;

@Data
public class TurboData {
	private double turboDurationMilliseconds;
	private int turboDurationTicks;
	private double turboFactor;
}
