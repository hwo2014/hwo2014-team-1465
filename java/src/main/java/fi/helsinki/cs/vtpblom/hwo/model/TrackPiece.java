package fi.helsinki.cs.vtpblom.hwo.model;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class TrackPiece {
	private int id;
	private boolean corner;
	private double length;
	private int radius;
	private double angle;
	@SerializedName("switch")
	private boolean laneSwitch;
}
