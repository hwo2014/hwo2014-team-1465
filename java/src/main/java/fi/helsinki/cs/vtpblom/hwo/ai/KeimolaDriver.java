package fi.helsinki.cs.vtpblom.hwo.ai;

import fi.helsinki.cs.vtpblom.hwo.model.Car;
import fi.helsinki.cs.vtpblom.hwo.model.CarState;
import fi.helsinki.cs.vtpblom.hwo.model.Game;
import fi.helsinki.cs.vtpblom.hwo.model.Track;
import fi.helsinki.cs.vtpblom.hwo.model.TrackPiece;

public class KeimolaDriver implements CarAI {
	private static final double EPSILON = 0.05;
	private Game game;
	private Decision decision = new Decision();
	private double targetSpeed;
	private double throttle;
	
	public KeimolaDriver(Game game) {
		this.game = game;
	}

	@Override
	public void analyze() {
		Car myCar = game.getMyCar();
		CarState currState = myCar.getCurrState();
		Track track = game.getTrack();
		TrackPiece currPiece = track.getPieces()[currState.getPieceIndex()];
		TrackPiece nextPiece = track.getPieces()[(currState.getPieceIndex()+1)%track.getPieces().length];
		double pieceLength = track.getPieceLength(currState.getPieceIndex(), currState.getStartLane());
		
		if (myCar.getPrevState() != null) {
			if (currPiece.isCorner() && !track.getPieces()[myCar.getPrevState().getPieceIndex()].isCorner()) {
				game.startRecording();
			}
			else if (!currPiece.isCorner() && track.getPieces()[myCar.getPrevState().getPieceIndex()].isCorner()) {
				game.stopRecording();
			}
		}

		if (currPiece.isCorner()) {
			if (pieceLength > 78) {
				this.targetSpeed = 6.9;
			}
			else {
				this.targetSpeed = 6.4;
			}
		}
		else {
			if (nextPiece.isCorner() && currState.getInPieceDistance() > (0.6 * pieceLength)) {
				this.targetSpeed = 6.4;
			}
			else {
				this.targetSpeed = 10.0;
			}
		}
		
		if (currState.getSpeed() > (this.targetSpeed)) {
			this.throttle = 0.0;
		}
		else {
			this.throttle = this.targetSpeed/10.0;
		}
	}

	@Override
	public Decision getDecision() {
		decision.setThrottle(this.throttle);
		return decision;
	}
}
