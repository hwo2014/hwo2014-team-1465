package fi.helsinki.cs.vtpblom.hwo.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class LaneNode {
	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	private TrackPiece piece;
	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	private Map<LaneDir, LaneNode> nextNodes = new HashMap<LaneDir, LaneNode>(3);
	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	private Map<LaneDir, Double> nodeDistances = new HashMap<LaneDir, Double>();

	private int lane;
	private int radius;
	private double length; // Default length of the lane when not switching
	
	public LaneNode(TrackPiece piece) {
		this.piece = piece;
	}
	
	public boolean isSwitch() {
		return piece.isLaneSwitch();
	}
	public boolean isCorner() {
		return piece.isCorner();
	}
	public double getAngle() {
		return piece.getAngle();
	}
	public int getId() {
		return piece.getId();
	}
	
	public void addNextNode(LaneDir dir, LaneNode node, double distance) {
		nextNodes.put(dir, node);
//		System.out.format("[LaneNode] My id: %d & lane %d | hasLeft? %s | hasRight? %s | hasStraight %s\n",
//				getId(), getLane(),
//				nextNodes.get(LaneDir.LEFT) != null, 
//				nextNodes.get(LaneDir.RIGHT) != null, 
//				nextNodes.get(LaneDir.STRAIGHT) != null);
	}
	
	public LaneNode getNextNode() {
		return getNextNode(LaneDir.STRAIGHT);
	}
	public LaneNode getNextNode(LaneDir dir) {
		return nextNodes.get(dir);
	}
	
	public double getDistanceToNext(LaneDir dir) {
		return nodeDistances.get(dir);
	}
	
	public List<LaneNode> getNodesToNextSwitch() {
		return getNodesToNextSwitch(LaneDir.STRAIGHT);
	}
	
	public List<LaneNode> getNodesToNextSwitch(LaneDir dir) {
		List<LaneNode> result = new ArrayList<LaneNode>();
		result = getNodesToNextSwitch(result, dir);
		return result;
	}
	
	private List<LaneNode> getNodesToNextSwitch(List<LaneNode> passed, LaneDir dir) {
		LaneNode next = nextNodes.get(dir);
		if (next == null) {
			return passed;
		}
		passed.add(next);
		if (next.isSwitch()) {
			return passed;
		}
		return next.getNodesToNextSwitch(passed, dir);
	}
	
//	public double getDistanceTo(LaneNode node) {
//		if (this.piece.getId() == node.piece.getId()) {
//			return 0;
//		}
//		
//	}
	
	public enum LaneDir {
		STRAIGHT,
		LEFT,
		RIGHT;
	}
}
