package fi.helsinki.cs.vtpblom.hwo.ai;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import noobbot.CarId;
import noobbot.CarPosData.PiecePos.Lane;
import fi.helsinki.cs.vtpblom.hwo.model.Car;
import fi.helsinki.cs.vtpblom.hwo.model.CarState;
import fi.helsinki.cs.vtpblom.hwo.model.Game;
import fi.helsinki.cs.vtpblom.hwo.model.LaneNode;
import fi.helsinki.cs.vtpblom.hwo.model.LaneNode.LaneDir;
import fi.helsinki.cs.vtpblom.hwo.physics.Environment;

public class RyanGosling implements CarAI {
	private Game game;
	private State state = State.MEASURING;
	private Decision decision = new Decision();
//	private double targetSpeed = 4;
	private double throttle = 1.0;
	
	public RyanGosling(Game game) {
		this.game = game;
	}

	@Override
	public Decision getDecision() {
		decision.setThrottle(throttle);
		return decision;
	}
	
	@Override
	public void analyze() {
		Car myCar = game.getMyCar();
		CarState carState = myCar.getCurrState();
		CarState prevState = myCar.getPrevState();
		Environment env =  game.getEnvironment();
		LaneNode currNode = game.getLaneNetwork().getCurrNode(myCar);

		if (state != State.CRASHED && carState.isCrashed()) {
			state = State.CRASHED;
			game.stopRecording();
		}
		
		// Switch lanes only if a) driven long enough b) is on track c) have arrived on a switch piece
		if (game.getGameTick() > 50 && carState.isOnTrack() && currNode.isSwitch() && !prevState.getPiece().isLaneSwitch()) {
			System.out.println("Should I change lanes at id " + currNode.getId());
			List<LaneNode> leftNodes = currNode.getNodesToNextSwitch(LaneDir.LEFT);
			List<LaneNode> straightNodes = currNode.getNodesToNextSwitch();
			straightNodes.add(0, currNode);
			List<LaneNode> rightNodes = currNode.getNodesToNextSwitch(LaneDir.RIGHT);
			
			makeLaneChangeDecision(myCar, game.getCars(), leftNodes, straightNodes, rightNodes);
		}
		
		// Reset switch signal
		if (currNode.isSwitch() && carState.getStartLane() != carState.getEndLane()) {
			this.decision.setSwitchSignal(SwitchSignal.NO_SIGNAL);
		}

		switch (state) {
		case MEASURING:
			if (game.getGameTick() > Environment.AIR_RES_MEASURING_TIME+2) {
				resetState(currNode);
			}
			break;
		case STRAIGHT :
//			throttle = env.getThrottleToSpeed(carState.getSpeed(), targetSpeed, 1.0);
			throttle = 1.0;
			double targetSpeed = calculateTargetSpeed(carState);
			throttle = env.getThrottleToSpeed(carState.getSpeed(), targetSpeed, carState.getTurboFactor());
			
			if (!currNode.isCorner() && carState.getPieceIndex() != prevState.getPieceIndex()) {
				if (carState.getTurboAvailable() != null && carState.getTurboRemaining() == 0) {
					if (isGoodSpotForTurbo(currNode)) {
						decision.setFireTurbo(true);
					}
				}
			}
			
//			LaneNode nextNode = currNode.getNextNode(LaneDir.STRAIGHT);
//			double brakeDist = env.getDistanceToSpeed(carState.getSpeed(), targetSpeed)*0.75;
//			if (nextNode.isCorner() && (currNode.getLength()-carState.getInPieceDistance() < brakeDist)) {
//				throttle = env.getThrottleToSpeed(carState.getSpeed(), targetSpeed+0.5, 1.0);
//			}
			if (currNode.isCorner()) {
//				throttle = env.getThrottleToSpeed(carState.getSpeed(), targetSpeed+0.5, 1.0);
				game.startRecording();
				state = State.CORNER;
			}
			
//			if (carState.getTurboAvailable() != null && carState.getTurboRemaining() == 0 && 
//					carState.getLap() == 1 && carState.getPieceIndex() == 35) {
//				decision.setFireTurbo(true);
//			}
			
			break;
		case CORNER :
			double currAngle = Math.abs(carState.getAngle());
			double angleDelta = currAngle - Math.abs(prevState.getAngle());
			targetSpeed = calculateTargetSpeed(carState);
//			if (currAngle < 1) {
//				tgtSpd += 0.5;
//			}
//			else {
//				tgtSpd += Math.max((0.5 - currAngle / env.getWarnAngle()), 0.0);
//			}
			throttle = env.getThrottleToSpeed(carState.getSpeed(), targetSpeed, carState.getTurboFactor());

			// Avoid too steep angles
			if (currAngle > env.getCrashAngle()) {
				throttle = 0.0;
			}
			else if (angleDelta > 3.0) {
				if ((env.getCrashAngle()-currAngle)/angleDelta < 5.0) {
					throttle = 0.0;
				}
			}
			if (!carState.getPiece().isCorner()) {
				game.stopRecording();
//				throttle = 1.0;
				state = State.STRAIGHT;
			}
			break;
		case CRASHED :
			throttle = 0.0;
			if (!carState.isCrashed()) {
				resetState(currNode);
			}
			break;
		}
		
	}
	
	private void resetState(LaneNode currNode) {
		if (currNode.isCorner()) {
			state = State.CORNER;
		}
		else {
			state = State.STRAIGHT;
		}
	}
	
	private void makeLaneChangeDecision(Car myCar, List<Car> allCars, List<LaneNode> toLeft, List<LaneNode> toStraight, List<LaneNode> toRight) {
		boolean leftAvailable = toLeft.size() > 0;
		boolean rightAvailable = toRight.size() > 0;
		Map<CarId, Double> carsLeft = getCarsOnNodes(allCars, toLeft, toStraight.get(0).getLength());
		Map<CarId, Double> carsStraight = getCarsOnNodes(allCars, toStraight, 0);
		Map<CarId, Double> carsRight = getCarsOnNodes(allCars, toRight, toStraight.get(0).getLength());
		double leftSpace = getFreeSpace(carsLeft);
		double straightSpace = getFreeSpace(carsStraight);
		double rightSpace = getFreeSpace(carsRight);
		boolean leftFree = leftAvailable && carsLeft.isEmpty();
		boolean straightFree = carsStraight.isEmpty();
		boolean rightFree = rightAvailable && carsRight.isEmpty();
		double leftDistance = getNodeListDist(toLeft);
		double straightDistance = getNodeListDist(toStraight);
		double rightDistance = getNodeListDist(toRight);
		
		SwitchSignal signal = SwitchSignal.NO_SIGNAL;
		if (!leftFree && !straightFree && !rightFree) {
			signal = getDirectionMax(leftSpace, straightSpace, rightSpace);
			System.out.format("All lanes blocked, chose %s\n", signal);
		}
		else if (leftFree && straightFree && rightFree) {
			signal = getDirectionMin(leftDistance, straightDistance, rightDistance);
			System.out.format("All lanes free, chose %s\n", signal);
		}
		else if (leftFree && straightFree) {
			signal = getDirectionMin(leftDistance, straightDistance, Double.MAX_VALUE);
			System.out.format("Left & straight free, chose %s\n", signal);
		}
		else if (leftFree && rightFree) {
			signal = getDirectionMin(leftDistance, Double.MAX_VALUE, rightDistance);
			System.out.format("Left & right free, chose %s\n", signal);
		}
		else if (straightFree && rightFree) {
			signal = getDirectionMin(Double.MAX_VALUE, straightDistance, rightDistance);
			System.out.format("Straight & right free, chose %s\n", signal);
		}
		else if (leftFree) {
			signal = SwitchSignal.LEFT;
			System.out.format("Only left free, chose %s\n", signal);
		}
		else if (rightFree) {
			signal = SwitchSignal.RIGHT;
			System.out.format("Only right free, chose %s\n", signal);
		}
		else {
			System.out.format("Didn't really expect this. Data was:\n" + 
					"Left avail: %s, Right avail: %s, Left space: %.1f, Cntr space: %.1f, Right space: %.1f\n" +
					"Left free: %s, Cntr free: %s, Right free: %s\n" +
					"Left dist: %.2f, Cntr dist: %.2f, Right dist: %.2f\n",
						leftAvailable, rightAvailable, leftSpace, straightSpace, rightSpace,
						leftFree, straightFree, rightFree,
						leftDistance, straightDistance, rightDistance);
			
		}
		
		if (signal != SwitchSignal.NO_SIGNAL) {
			this.decision.setSwitchSignal(signal);
		}
		
	}
	
	private double getNodeListDist(List<LaneNode> nodes) {
		double dist = 0;
		for (LaneNode ln : nodes) {
			dist += ln.getLength();
		}
		return dist;
	}
	
	private Map<CarId, Double> getCarsOnNodes(List<Car> allCars, List<LaneNode> nodes, double initDistance) {
		Map<CarId, Double> carsFound = new HashMap<CarId, Double>();
		double dist = initDistance;
		for (LaneNode node : nodes) {
			for (Car c : allCars) {
				LaneNode cNode = game.getLaneNetwork().getCurrNode(c);
				if (c.getCurrState().isOnTrack() && !c.getId().equals(game.getMyCar().getId())) {
					if (cNode.getId() == node.getId() && cNode.getLane() == node.getLane()) {
						carsFound.put(c.getId(), dist + c.getCurrState().getInPieceDistance());
					}
				}
			}
			dist += node.getLength();
		}
		return carsFound;
	}

	private double getFreeSpace(Map<CarId, Double> carDistances) {
		double min = Double.MAX_VALUE;
		for (double dist : carDistances.values()) {
			if (dist < min) {
				min = dist;
			}
		}
		return min;
	}
	
	private SwitchSignal getDirectionMax(double left, double straight, double right) {
		if (left > straight && left > right) {
			return SwitchSignal.LEFT;
		}
		if (straight > right) {
			return SwitchSignal.NO_SIGNAL;
		}
		return SwitchSignal.RIGHT;
	}
	
	private SwitchSignal getDirectionMin(double left, double straight, double right) {
		if (left < straight && left < right) {
			return SwitchSignal.LEFT;
		}
		if (straight < right) {
			return SwitchSignal.NO_SIGNAL;
		}
		return SwitchSignal.RIGHT;
	}
	
	private double getSlowestCarAverage(List<Car> cars) {
		double slowest = Double.MAX_VALUE;
		for (Car c : cars) {
			double avg = c.getAverageSpeed();
			if (avg < slowest) {
				slowest = avg;
			}
		}
		return slowest;
	}
	
	private boolean isGoodSpotForTurbo(LaneNode currNode) {
//		double checkDistance = game.getEnvironment().getDistanceToSpeed(15, Environment.SLOWEST_SPEED);
		double checkDistance = 350;
		List<LaneNode> nextNodes = game.getLaneNetwork().getNodesAhead(game.getMyCar(), checkDistance);
		for (LaneNode ln : nextNodes) {
			if (ln.isCorner() && ln.getRadius() < 210) {
				return false;
			}
		}
		return true;
	}
	
	private double calculateTargetSpeed(CarState currState) {
		double maxBrakeDist = game.getEnvironment().getDistanceToSpeed(currState.getSpeed(), Environment.SLOWEST_SPEED);
		List<LaneNode> nextNodes = game.getLaneNetwork().getNodesAhead(game.getMyCar(), maxBrakeDist);
		return findMaxSpeed(nextNodes);
	}
	
	private double findMaxSpeed(List<LaneNode> nextNodes) {
		double currSpeed = game.getMyCar().getCurrState().getSpeed();
		double inPieceDist = game.getMyCar().getCurrState().getInPieceDistance();
		double dist = 0.0;
		double maxSpeed = Environment.HIGHEST_SPEED;
		for (LaneNode node : nextNodes) {
			double nodeTargetSpeed = game.getEnvironment().getNodeSpeed(node);
			double nodeDistance = dist - inPieceDist + node.getLength()*0.2;
			double slowDist = game.getEnvironment().getDistanceToSpeed(currSpeed, nodeTargetSpeed);
			if (slowDist > nodeDistance) {
				if (nodeTargetSpeed < maxSpeed) {
					maxSpeed = nodeTargetSpeed;
				}
			}
			dist += node.getLength();
		}
		return maxSpeed;
	}

	private enum State {
		MEASURING,
		STRAIGHT,
		CORNER,
		CRASHED;
	}
}
