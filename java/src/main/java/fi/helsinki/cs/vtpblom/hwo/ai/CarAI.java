package fi.helsinki.cs.vtpblom.hwo.ai;

import fi.helsinki.cs.vtpblom.hwo.model.Game;

public interface CarAI {
	public void analyze();
	public Decision getDecision();
}
