package fi.helsinki.cs.vtpblom.hwo.model;

public enum RaceState {
	CREATED,
	INIT,
	START,
	FINISHED,
	END,
	GAME_OVER
}
