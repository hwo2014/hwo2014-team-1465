package fi.helsinki.cs.vtpblom.hwo.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

import lombok.Data;
import lombok.ToString;
import noobbot.CarId;
import noobbot.CarPosData;

@Data
@ToString(exclude={"posHistory","game"})
public class Car {
	private transient final Game game;
	private final CarId id;

	// Dimensions
	private double length;
	private double width;
	private double guideFlagPos;
	
	// Car details
	private CarState currState;
	private CarState prevState;
	private transient List<CarState> posHistory = new ArrayList<CarState>();

	// Statistics
	private List<LapTime> lapTimes = new ArrayList<LapTime>();
	private RaceTime raceTime;
	private double distanceTraveled = 0.0;
	private long ticksAlive = 0L;
	private double maxSpeed = 0.0;
	private double maxAngle = 0.0;
	
	public void addData(long gameTick, CarPosData data) {
		if (currState == null || gameTick == 0) { // First data
			System.out.println("Car state reset?");
			CarState newState = parseState(data);
			newState.setTick(gameTick);
			currState = newState;
			posHistory.add(newState);
			ticksAlive++;
		}
		else {
			CarState newState = parseState(data);
			newState.setTick(gameTick);
			newState.setCrashed(currState.isCrashed());
			newState.setFinished(currState.isFinished());
			newState.setTurboAvailable(currState.getTurboAvailable());
			if (currState.getTurboRemaining() > 0) {
				newState.setTurboRemaining(currState.getTurboRemaining()-1);
				newState.setTurboFactor(currState.getTurboFactor());
			}
			performCalculations(currState, newState);
			
			prevState = currState;
			currState = newState;
			posHistory.add(newState);
		}
	}
	
	public void addLapData(LapTime lapTime, RaceTime raceTime) {
		this.lapTimes.add(lapTime);
		this.raceTime = raceTime;
		System.out.format("%s completed a lap in %.3f secs [fastest %.3f]\n",
				id.name,
				lapTime.getMillis()/1000.0,
				getFastestLap().getMillis()/1000.0);
	}
	
	public LapTime getFastestLap() {
		LapTime fastestLap = null;
		for (LapTime lt : lapTimes) {
			if (fastestLap == null || lt.getMillis() < fastestLap.getMillis()) {
				fastestLap = lt;
			}
		}
		return fastestLap;
	}
	
	public List<CarState> getLastNStates(int n, boolean onlyAlive) {
		List<CarState> states = new ArrayList<CarState>(n);
		for (ListIterator<CarState> iter = posHistory.listIterator(posHistory.size());
				iter.hasPrevious() && states.size() < n;) {
			CarState cs = iter.previous();
			if (!onlyAlive || cs.isOnTrack()) {
				states.add(cs);
			}
		}
		Collections.reverse(states); // To correct order
		return states;
	}
	
	public double getAverageSpeed() {
		if (ticksAlive > 0) {
			return distanceTraveled/ticksAlive;
		}
		return 0.0;
	}
		
	private CarState parseState(CarPosData data) {
		CarState state = new CarState();
		state.setAngle(data.angle);
		state.setPieceIndex(data.piecePosition.pieceIndex);
		state.setPiece(this.game.getTrack().getPieces()[state.getPieceIndex()]);
		state.setInPieceDistance(data.piecePosition.inPieceDistance);
		state.setLap(data.piecePosition.lap);
		state.setStartLane(data.piecePosition.lane.startLaneIndex);
		state.setEndLane(data.piecePosition.lane.endLaneIndex);
		return state;
	}
	
	private void performCalculations(CarState prev, CarState curr) {
		double distDelta = getDeltaDistance(prev, curr);
		long tickDelta = curr.getTick()-prev.getTick();
		double newSpeed = distDelta/tickDelta; // TODO: DBZ error possible?
		curr.setSpeed(newSpeed);
		if (newSpeed > 0.00001) { // Don't want deceleration data when speed suddenly 0
			curr.setAcceleration(newSpeed-prev.getSpeed());
		}
		if (!curr.isCrashed() && !curr.isFinished()) {
			if (newSpeed > maxSpeed) {
				maxSpeed = newSpeed;
			}
			if (Math.abs(curr.getAngle()) > maxAngle) {
				maxAngle = Math.abs(curr.getAngle());
			}
			this.ticksAlive += tickDelta;
			this.distanceTraveled += distDelta;
		}
	}
	
	private double getDeltaDistance(CarState prev, CarState curr) {
		double distDelta = 0.0;
		if (prev.getPieceIndex() != curr.getPieceIndex()) {
			double prevPieceLength = game.getTrack().getPieceLength(prev.getPieceIndex(), prev.getEndLane());
			double endOfPiece = prevPieceLength-prev.getInPieceDistance();
			if (endOfPiece > 0) {
				distDelta += endOfPiece;
			}
			else {
				System.out.format("NOTE! Piece len calc error. Len: %.3f, InPiece: %.3f\n", prevPieceLength, prev.getInPieceDistance());
			}
			distDelta += curr.getInPieceDistance();
		}
		else {
			distDelta = curr.getInPieceDistance()-prev.getInPieceDistance();
		}
		return distDelta;
	}
		
//	public void resetStatistics() {
//		speed = 0.0;
//		maxSpeed = 0.0;
//		distanceTraveled = 0.0;
//		ticksAlive = 0L;
//	}
}
