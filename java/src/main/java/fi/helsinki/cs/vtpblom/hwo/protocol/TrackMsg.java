package fi.helsinki.cs.vtpblom.hwo.protocol;

import fi.helsinki.cs.vtpblom.hwo.model.TrackPiece;

public class TrackMsg {
	public String id;
	public String name;
	public TrackPiece[] pieces;
	public LaneMsg[] lanes;
}
