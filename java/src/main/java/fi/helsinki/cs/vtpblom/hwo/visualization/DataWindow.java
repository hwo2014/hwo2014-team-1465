package fi.helsinki.cs.vtpblom.hwo.visualization;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JPanel;

import fi.helsinki.cs.vtpblom.hwo.ai.CarAI;
import fi.helsinki.cs.vtpblom.hwo.model.Car;
import fi.helsinki.cs.vtpblom.hwo.model.CarState;
import fi.helsinki.cs.vtpblom.hwo.model.Game;
import fi.helsinki.cs.vtpblom.hwo.model.LaneNode;
import fi.helsinki.cs.vtpblom.hwo.model.TrackPiece;

public class DataWindow extends JFrame implements Observer {
	private Game game;
	private CarAI ai;
	
	private InfoPanel panel;
	
	public DataWindow() {
		super("HWO2014 visualization");
	}
	
	public void setGame(Game game) {
		this.game = game;
		game.addObserver(this);
	}
	
	public void setAI(CarAI ai) {
		this.ai = ai;
	}

	@Override
	public void update(Observable o, Object arg) {
		panel.invalidate();
		panel.repaint();
	}
	
	public void start() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.panel = new InfoPanel();
		this.panel.setBackground(Color.DARK_GRAY);
		this.getContentPane().add(panel);
		this.setResizable(false);
		this.pack();
		this.setVisible(true);
	}
	
	class InfoPanel extends JPanel {
		private static final long serialVersionUID = -6257603563130019787L;
		private int width = 800;
		private int height = 600;
		private Dimension preferredDim = new Dimension(width,height);
		@Override
		public Dimension getPreferredSize() {
			return preferredDim;
		}
		
		@Override
		public void paint(Graphics g) {
			super.paint(g);
			if (game.getGameTick() > 2) {
				Graphics2D g2d = (Graphics2D)g;
				g2d.setColor(Color.WHITE);
				CarState myCar = game.getMyCar().getCurrState();
				g2d.drawString(String.format("Tick: %4d", game.getGameTick()), 10, 15);
				g2d.drawString(String.format("Throttle: %.2f", ai.getDecision().getThrottle()), 10, 30);
				TrackPiece currPiece = game.getTrack().getPieces()[myCar.getPieceIndex()];
				g2d.drawString(String.format("Current piece: id %2d / dist: %5.2f [len %6.2f / rad %3d ang %2.0f]",
						myCar.getPieceIndex(), myCar.getInPieceDistance(),
						currPiece.getLength(), currPiece.getRadius(), currPiece.getAngle()), 10, 45);
				LaneNode node = game.getLaneNetwork().getCurrNode(game.getMyCar());
				g2d.drawString(String.format("Current node: id    / dist: %5.2f [len %6.2f / rad %3d ang %2.0f] []",
						myCar.getInPieceDistance(),
						node.getLength(), node.getRadius(), node.getAngle()), 10, 60);
				g2d.drawString(String.format("Speed: %4.2f | Accel: %4.2f",
						myCar.getSpeed(), myCar.getAcceleration()), 10, 75);
			}
			
			
//			game.getMyCar().getPosHistory()
		}
	}

}
