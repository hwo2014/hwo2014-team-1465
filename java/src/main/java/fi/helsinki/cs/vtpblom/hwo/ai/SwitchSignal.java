package fi.helsinki.cs.vtpblom.hwo.ai;

public enum SwitchSignal {
	NO_SIGNAL,
	LEFT,
	RIGHT,
	RECEIVED,
	SWITCHED;
}
