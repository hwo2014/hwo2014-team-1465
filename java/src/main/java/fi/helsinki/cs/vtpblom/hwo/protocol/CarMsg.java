package fi.helsinki.cs.vtpblom.hwo.protocol;

import noobbot.CarId;

public class CarMsg {
	public CarId id;
	public DimensionMsg dimensions;
}
